import bson
import io
from PIL import Image
from torch.utils.data import Dataset
import numpy as np
from torchvision import transforms
import random
import logging

class DatasetDB(Dataset):
    def __init__(self, classes, date, sample=False):
        self._label_dtype = np.int32

        self.date = date
        filepath = 'logs/' + self.date.isoformat() + '.log'
        logging.basicConfig(filename=filepath, format='%(asctime)s %(message)s', level=logging.INFO)

        self.classes = classes
        self.label_dictionary = {k: v for v, k in enumerate(classes)}

        self.data = []
        self.input_size = 224

        self.transform = transforms.Compose(
            [
                transforms.Resize(self.input_size),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
            ]
        )

        with open('../db/products.bson', 'rb') as f:
            coll_raw = f.read()
        
        self.data = []

        self.products = bson.decode_all(coll_raw)

        if sample:
            print("SAMPLE")
            randIndex = random.sample(range(len(self.products)), sample)
            randIndex.sort()
            self.products = [self.products[i] for i in randIndex]

        for i, product in enumerate(self.products):
            images = product.pop("images", None)
            for image in images:
                img = self.pil_loader(image)
                if img.mode == "RGB":
                    image_data = {"image": image}
                    image_data.update(product)
                    self.data.append(image_data)

    def class_count(self):
        class_count = {i: i for i in [i for i in range(len(self.classes))]}

        for product in self.products:
            image_class = self.label_dictionary[product["category"]]
            image_length = product["imageLength"]
            class_count[image_class] += image_length
        
        return class_count


    def pil_loader(self, f):
        buf = io.BytesIO(f)
        img = Image.open(buf)
        return img
    
    def __len__(self):
        return len(self.data)

    def __getitem__(self, i):
        product = self.data[i]
        _id = product["_id"]

        img = product["image"]
        img = self.pil_loader(img)

        if img.mode == "RGBA":
            imgCorrect = Image.new("RGB", img.size, (255, 255, 255))
            imgCorrect.paste(img, mask=img.split()[3])  # 3 is the alpha channel
            img = imgCorrect

        if self.transform:
            img = self.transform(img)

        label = self.label_dictionary[product["category"]]

        return (img, label)