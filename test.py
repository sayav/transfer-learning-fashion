from __future__ import print_function
from __future__ import division
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
from torch.utils.data import Dataset, DataLoader, WeightedRandomSampler, SubsetRandomSampler
from dataset import DatasetDB
import matplotlib.pyplot as plt
import time
import os
import copy
import random

import logging

import seaborn as sn
import pandas as pd
from sklearn.metrics import confusion_matrix as cm

import os
if not os.path.exists('logs'):
    os.makedirs('logs')
if not os.path.exists('logs/archive'):
    os.makedirs('logs/archive')
from os.path import isfile, join
log_files = [f for f in os.listdir('logs/') if isfile(join('logs/', f))]
for file in log_files:
    log_full_path = 'logs/' + file
    log_new_path = 'logs/archive/' + file
    os.rename(log_full_path, log_new_path)

import datetime
date = datetime.datetime.now()
logging.basicConfig(filename='logs/' + date.isoformat() +'.log', format='%(asctime)s %(message)s', level=logging.INFO)

logging.info("PyTorch Version: " + torch.__version__)
logging.info("Torchvision Version: " + torchvision.__version__)

# Models to choose
model_name = "resnet-18"

classes = [
    "coats",
    "dresses",
    "jumpsuits",
    "shirts",
    "shorts",
    "skirts",
    "suits",
    "sweaters",
    "tops",
    "trousers",
]

# Number of classes in the dataset
num_classes = len(classes)

# Batch size for training (change depending on how much memory you have)
batch_size = 500

def train_model(model, dataloader, criterion):
    since = time.time()

    best_acc = 0.0

    total_preds = []
    total_labels = []

    model.eval()  # Set model to evaluate mode

    running_loss = 0.0
    running_corrects = 0

    # Count iterations over dataset
    product_count = 0
    product_percent = 0

    with torch.no_grad():
        for data in dataloader:
            inputs, labels = data

            inputs = inputs.to(device)
            labels = labels.to(device)

            # Get model outputs and calculate loss
            outputs = model(inputs)
            loss = criterion(outputs, labels)

            _, preds = torch.max(outputs, 1)

            # statistics
            running_loss += loss.item() * inputs.size(0)
            running_corrects += torch.sum(preds == labels.data)

            total_preds.extend(preds.tolist())
            total_labels.extend(labels.tolist())


            product_count += batch_size
            logging.info(product_count)
        
    loss = running_loss / test_length
    acc = running_corrects.double() / test_length

    logging.info("Loss: {:.4f} Acc: {:.4f}".format(loss, acc))

    time_elapsed = time.time() - since
    logging.info(
        "Testing complete in {:.0f}m {:.0f}s".format(
            time_elapsed // 60, time_elapsed % 60
        )
    )

    # load best model weights
    return model, total_preds, total_labels

######################################################################
#  Initialize the model for this run
# ---------
if model_name == "resnet-18":
    model_ft = models.resnet18(pretrained=True)
    num_ftrs = model_ft.fc.in_features
    model_ft.fc = nn.Linear(num_ftrs, num_classes)
    state_dict = torch.load("models/resnet-18.pth")["net"]

    from collections import OrderedDict
    new_state_dict = OrderedDict()
    for k, v in state_dict.items():
        name = k[7:] # remove `module.`
        new_state_dict[name] = v

    model_ft.load_state_dict(new_state_dict)

######################################################################
# Load Data
# ---------

logging.info("Initializing Datasets and Dataloaders...")

dataset = DatasetDB(classes, date)

test_split = 0.05
test_start_i = int(len(dataset) * (1 - test_split))
test_length = int(len(dataset) * test_split)

# Randomize and split dataset
indices = np.random.RandomState(seed=42).permutation(list(range(len(dataset))))

test_indices = indices[test_start_i:]

test_sampler = SubsetRandomSampler(test_indices)

# Create training and validation dataloaders
dataloader = DataLoader(
    dataset, batch_size=batch_size, shuffle=False, num_workers=4, sampler=test_sampler
)

logging.info("Finished Loading Datasets")

# Detect if we have a GPU available
if torch.cuda.is_available():
    logging.info("GPU Enabled")
    model_ft = nn.DataParallel(model_ft, device_ids=[0, 1])
    device = torch.device("cuda:0")
else:
    logging.info("CPU Enabled")
    device = torch.device("cpu")



######################################################################
# Create the Optimizer
# --------------------

# Send the model to GPU/CPU
model_ft = model_ft.to(device) 

# Setup the loss fxn
criterion = nn.CrossEntropyLoss()

# Train and evaluate
model_ft, preds, labels = train_model(model_ft, dataloader, criterion)

######################################################################
# Confusion Matrix
# --------------------
confusion_matrix = cm(labels, preds)
print(confusion_matrix)

df_cm = pd.DataFrame(confusion_matrix, index = classes, columns = classes)
plt.figure(figsize = (10,7))
sn.heatmap(df_cm, annot=True, fmt='g')
path = "results/" + model_name + "confusion-matrix.png"
plt.savefig("results/confusion-matrix.png", bbox_inches="tight")
