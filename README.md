# Method

* [Introduction](#markdown-header-introduction)
* [Data Loading & Processing](#markdown-header-data-loading-processing)
    * [Initializing Dataset](#markdown-header-initializing-dataset)
    * [Retrieving Images](#markdown-header-retrieving-images)
* [Transfer Learning](#markdown-header-transfer-learning)
    * [Parameters](#markdown-header-parameters)
    * [Initializing Pre-Trained Model](#markdown-header-initializing-pre-trained-model)
    * [Optimizer](#markdown-header-optimizer)
    * [Model Training & Validation](#markdown-header-model-training-evaluation)
* [Results](#markdown-header-results)

## Introduction

This repository gives details on how to train a custom fashion dataset in PyTorch with models pretrained with the 1000 class ImageNet dataset. The method follows the PyTorch documentation for ["Finetuning TorchVision Models"](https://pytorch.org/tutorials/beginner/finetuning_torchvision_models_tutorial.html) written by Nathan Inkawhich.

## Data Loading & Processing

PyTorch has an abstract class, `torch.utils.data.Dataset`, representing a dataset. When using a custom dataset, this class in inherited and several methods are overridden.

The code for data loading and processing is found within `dataset.py`.

### Initializing Dataset

The dataset is a BSON (binary JSON) file, containing approximately 500,000 items. In each item there are several images which must be extracted.

Initially the BSON file is converted into JSON so that it can be read by Python.

```python
with open('../db/products.bson', 'rb') as f:
  coll_raw = f.read()

self.products = bson.decode_all(coll_raw)
```

`self.products` is a list that contains all of the product information. The entire list is iterated over to extract the images in each product. The image channel is also checked in this process to ensure that only RGB images are loaded into the dataset. This prevents some corrupted files from being loaded into the dataset.

```python
for i, product in enumerate(self.products):
  images = product.pop("images", None)
  for image in images:
    img = self.pil_loader(image)
    if img.mode == "RGB":
      image_data = {"image": image}
      image_data.update(product)
      self.data.append(image_data)
```

### Retrieving Images

The `pil_loader` method converts the images in Base64 binary format into a format that can be readable by PIL, which is used to load the image into the GPU.

The `__len__` method returns the length of the dataset (approx. 1.5 million).

The `__getitem__` method retrieves images and class information. It also utilizes PyTorch's transforms module to perform augemtations on the image.

The transforms performed on the images are given below:

```python
self.transform = transforms.Compose(
  [
    transforms.RandomHorizontalFlip(),
    transforms.ToTensor(),
    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
  ]
)
```

### Sampling Data

In order to evaluate the performance of the dataset the dataset is split into three subsets:

- **Training Dataset** - Weights are updated to fit the model.
- **Evaluation Dataset** - The loss function and acuraccy are measured to measure the performance of the ConvNet.
- **Test Dataset** - The weights that lead to the best performance of the dataset are evaluated on new data to get final acuraccy values.

PyTorch has a sampler, `SubsetRandomSampler`, which takes indices to split the dataset. Random indices are generated using a NumPy method from `np.random` using defined ratio splits.

```python
train_split = 0.7
train_split = 0.7
validate_split = 0.25
test_split = 0.05
train_length = int(len(dataset) * train_split)
val_length = int(len(dataset) * validate_split)

# Randomize and split dataset
indices = list(range(len(dataset)))
np.random.shuffle(indices)
train_indices = indices[:train_length]
val_indices = indices[train_length : val_length + train_length]
test_indices = indices[val_length + train_length:]

train_sampler = SubsetRandomSampler(train_indices)
eval_sampler = SubsetRandomSampler(val_indices)

# Create training and validation dataloaders
dataloaders_dict = {}
dataloaders_dict["train"] = DataLoader(
  dataset, batch_size=batch_size, shuffle=False, num_workers=4, sampler=train_sampler
)
dataloaders_dict["val"] = DataLoader(
  dataset, batch_size=batch_size, shuffle=False, num_workers=4, sampler=eval_sampler
)
```

# Transfer Learning

There are two different methods that can be used to perform transfer learning, which are briefly defined below:

- **Finetuning** - Updating all of the weights of a pre-trained model.
- **Feature Extraction** - Updating only the weights of the final layer.

This project uses finetuning which converges faster.

## Parameters

The following parameters are defined to be used in the training model.

```python
model_name = "resnet"

# Batch size for training (change depending on how much memory you have)
batch_size = 1000

# Number of epochs to train for
num_epochs = 30

# When True only update the reshaped layer params
feature_extract = True
```

- The model name refers to the pre-trained model that is used. In the case of this method, ResNet is used.
- The batch size is set at 1000 due to the fact that the dataset is large.
- The number of epochs refer to the number of times in which the model is evaluated.

## Initializing Pre-Trained Model

The final fully connected layer, `model_ft.fc`, is reinitialized to be a linear layer with 512 input features and 10 output features referring to the number of classes.

```python
if model_name == "resnet":
  model_ft = models.resnet50(pretrained=use_pretrained)
  set_parameter_requires_grad(model_ft, feature_extract)
  num_ftrs = model_ft.fc.in_features
  model_ft.fc = nn.Linear(num_ftrs, num_classes)
```

## Optimizer

The code for the optimizer ensures that only the weights of the final layer are updated.

The Adam optimization algorithm is used, which has a default learning rate of 0.001.

`nn.CrossEntropyLoss()` combines `nn.LogSoftmax()` and `nn.NLLLoss()` in one single class.

```python
params_to_update = model_ft.parameters()
logging.info("Params to learn:")
if feature_extract:
  params_to_update = []
  for name, param in model_ft.named_parameters():
    if param.requires_grad == True:
      params_to_update.append(param)
      logging.info(name)

optimizer_ft = optim.Adam(params_to_update)
criterion = nn.CrossEntropyLoss()
```

The parameters that are updated are:

```
fc.weight
fc.bias
```

## Model Training & Validation

The inputs are fed into the ResNet model, whose behavior is dependant on whether the dataset in either training or validation.

The loss function and accuracy scores are computed and logged. The results can be found [here](#results).

```python
def train_model(model, dataloaders, criterion, optimizer, num_epochs):

  best_model_wts = copy.deepcopy(model.state_dict())
  best_acc = 0.0

  for epoch in range(num_epochs):
  logging.info("Epoch {}/{}".format(epoch, num_epochs - 1))
  logging.info("-" * 10)

  # Each epoch has a training and validation phase
  for phase in ["train", "val"]:
    if phase == "train":
      model.train()  # Set model to training mode
    else:
      model.eval()  # Set model to evaluate mode

    running_loss = 0.0
    running_corrects = 0

    # Count iterations over dataset
    product_count = 0
    product_percent = 0

    # Iterate over data.
    for inputs, labels in dataloaders[phase]:
      inputs = inputs.to(device)
      labels = labels.to(device)

      # zero the parameter gradients
      optimizer.zero_grad()

      # forward
      # track history if only in train
      with torch.set_grad_enabled(phase == "train"):
        # Get model outputs and calculate loss
        outputs = model(inputs)
        loss = criterion(outputs, labels)

        _, preds = torch.max(outputs, 1)

        # backward + optimize only if in training phase
        if phase == "train":
          loss.backward()
          optimizer.step()

      # statistics
      running_loss += loss.item() * inputs.size(0)
      running_corrects += torch.sum(preds == labels.data)

    epoch_loss = running_loss / len(dataloaders[phase].dataset)
    epoch_acc = running_corrects.double() / len(dataloaders[phase].dataset)

    logging.info("{} Loss: {:.4f} Acc: {:.4f}".format(phase, epoch_loss, epoch_acc))

  time_elapsed = time.time() - since
  logging.info(
      "Training complete in {:.0f}m {:.0f}s".format(
          time_elapsed // 60, time_elapsed % 60
      )
  )
  logging.info("Best val Acc: {:4f}".format(best_acc))

  # load best model weights
  model.load_state_dict(best_model_wts)
  return model, val_acc_history
```

# Results

## ResNet-18
![ ](results/resnet-18-training-loss.png)
![ ](results/resnet-18-training-accuracy.png)
![ ](results/resnet-18-validation-loss.png)
![ ](results/resnet-18-validation-accuracy.png)

## ResNet-50
![ ](results/resnet-50-training-loss.png)
![ ](results/resnet-50-training-accuracy.png)
![ ](results/resnet-50-validation-loss.png)
![ ](results/resnet-50-validation-accuracy.png)