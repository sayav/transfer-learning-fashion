from __future__ import print_function
from __future__ import division
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
from torch.utils.data import Dataset, DataLoader, WeightedRandomSampler, SubsetRandomSampler
from dataset import DatasetDB
import matplotlib.pyplot as plt
import time
import os
import copy
import random

import logging

import os
if not os.path.exists('logs'):
    os.makedirs('logs')
if not os.path.exists('logs/archive'):
    os.makedirs('logs/archive')
from os.path import isfile, join
log_files = [f for f in os.listdir('logs/') if isfile(join('logs/', f))]
for file in log_files:
    log_full_path = 'logs/' + file
    log_new_path = 'logs/archive/' + file
    os.rename(log_full_path, log_new_path)

import datetime
date = datetime.datetime.now()
logging.basicConfig(filename='logs/' + date.isoformat() +'.log', format='%(asctime)s %(message)s', level=logging.INFO)

logging.info("PyTorch Version: " + torch.__version__)
logging.info("Torchvision Version: " + torchvision.__version__)

# Models to choose from [resnet, alexnet, vgg, squeezenet, densenet]
model_name = "resnet"

classes = [
    "coats",
    "dresses",
    "jumpsuits",
    "shirts",
    "shorts",
    "skirts",
    "suits",
    "sweaters",
    "tops",
    "trousers",
]

# Number of classes in the dataset
num_classes = len(classes)

# Batch size for training (change depending on how much memory you have)
batch_size = 1000

# Number of epochs to train for
num_epochs = 90

# Flag for feature extracting. When False, we finetune the whole model,
#   when True we only update the reshaped layer params
feature_extract = True


def train_model(model, dataloaders, criterion, optimizer, num_epochs):
    since = time.time()

    val_acc_history = []

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0

    for epoch in range(num_epochs):
        logging.info("Epoch {}/{}".format(epoch, num_epochs - 1))
        logging.info("-" * 10)

        # Each epoch has a training and validation phase
        for phase in ["train", "val"]:
            if phase == "train":
                model.train()  # Set model to training mode
            else:
                model.eval()  # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            # Count iterations over dataset
            product_count = 0
            product_percent = 0

            # Iterate over data.
            for inputs, labels in dataloaders[phase]:
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == "train"):
                    # Get model outputs and calculate loss
                    outputs = model(inputs)
                    loss = criterion(outputs, labels)

                    _, preds = torch.max(outputs, 1)

                    # backward + optimize only if in training phase
                    if phase == "train":
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)
               
                #make better logs

                product_count += batch_size
                logging.info(product_count)

            epoch_loss = running_loss / lengths[phase]
            epoch_acc = running_corrects.double() / lengths[phase]

            logging.info("{} Loss: {:.4f} Acc: {:.4f}".format(phase, epoch_loss, epoch_acc))

            # deep copy the model
            if phase == "val" and epoch_acc > best_acc:
                if not os.path.isdir("models"):
                    os.mkdir("models")

                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(model.state_dict())

                logging.info("Saving..")
                state = {
                    "net": model.state_dict(),
                    "epoch_acc": epoch_acc,
                    "epoch": epoch,
                }
                torch.save(state, "models/model.pth")

            if phase == "val":
                val_acc_history.append(epoch_acc)

    time_elapsed = time.time() - since
    logging.info(
        "Training complete in {:.0f}m {:.0f}s".format(
            time_elapsed // 60, time_elapsed % 60
        )
    )
    logging.info("Best val Acc: {:4f}".format(best_acc))

    # load best model weights
    model.load_state_dict(best_model_wts)
    return model, val_acc_history


######################################################################
# Set Model Parameters’ .requires_grad attribute
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def set_parameter_requires_grad(model, feature_extracting):
    if feature_extracting:
        for param in model.parameters():
            param.requires_grad = False


######################################################################
# Initialize and Reshape the Networks
# -----------------------------------


def initialize_model(model_name, num_classes, feature_extract, use_pretrained=True):
    # Initialize these variables which will be set in this if statement. Each of these
    #   variables is model specific.
    model_ft = None
    input_size = 0

    if model_name == "resnet":
        model_ft = models.resnet18(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        num_ftrs = model_ft.fc.in_features
        # model_ft.fc = nn.Sequential(nn.Linear(num_ftrs, 100), 
        #                             nn.ReLU(),
        #                             nn.Linear(100, num_classes))
        model_ft.fc = nn.Linear(num_ftrs, num_classes)

    elif model_name == "alexnet":
        model_ft = models.alexnet(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        num_ftrs = model_ft.classifier[6].in_features
        model_ft.classifier[6] = nn.Linear(num_ftrs, num_classes)

    elif model_name == "vgg":
        model_ft = models.vgg11_bn(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        num_ftrs = model_ft.classifier[6].in_features
        model_ft.classifier[6] = nn.Linear(num_ftrs, num_classes)

    elif model_name == "squeezenet":
        model_ft = models.squeezenet1_0(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        model_ft.classifier[1] = nn.Conv2d(
            512, num_classes, kernel_size=(1, 1), stride=(1, 1)
        )
        model_ft.num_classes = num_classes

    elif model_name == "densenet":
        model_ft = models.densenet121(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        num_ftrs = model_ft.classifier.in_features
        model_ft.classifier = nn.Linear(num_ftrs, num_classes)

    else:
        logging.info("Invalid model name, exiting...")
        exit()

    return model_ft


# Initialize the model for this run
model_ft = initialize_model(
    model_name, num_classes, feature_extract, use_pretrained=True
)

######################################################################
# Load Data
# ---------

logging.info("Initializing Datasets and Dataloaders...")

dataset = DatasetDB(classes, date, sample=50000)
# print(dataset.class_count())
# print(len(dataset))

train_split = 0.7
validate_split = 0.25
test_split = 0.05
train_length = int(len(dataset) * train_split)
val_length = int(len(dataset) * validate_split)

# Randomize and split dataset
indices = np.random.RandomState(seed=42).permutation(list(range(len(dataset))))

train_indices = indices[:train_length]
val_indices = indices[train_length : val_length + train_length]
test_indices = indices[val_length + train_length:]

train_sampler = SubsetRandomSampler(train_indices)
eval_sampler = SubsetRandomSampler(val_indices)

# Create training and validation dataloaders
dataloaders_dict = {}
dataloaders_dict["train"] = DataLoader(
    dataset, batch_size=batch_size, shuffle=False, num_workers=4, sampler=train_sampler
)
dataloaders_dict["val"] = DataLoader(
    dataset, batch_size=batch_size, shuffle=False, num_workers=4, sampler=eval_sampler
)

lengths = {"train": train_length, "val": val_length}

logging.info("Finished Loading Datasets")

# Detect if we have a GPU available
if torch.cuda.is_available():
    logging.info("GPU Enabled")
    model_ft = nn.DataParallel(model_ft, device_ids=[0, 1])
    device = torch.device("cuda:0")
else:
    logging.info("CPU Enabled")
    device = torch.device("cpu")



######################################################################
# Create the Optimizer
# --------------------

# Send the model to GPU/CPU
model_ft = model_ft.to(device) 

# Gather the parameters to be optimized/updated in this run. If we are
#  finetuning we will be updating all parameters. However, if we are
#  doing feature extract method, we will only update the parameters
#  that we have just initialized, i.e. the parameters with requires_grad
#  is True.
params_to_update = model_ft.parameters()
logging.info("Params to learn:")
if feature_extract:
    params_to_update = []
    for name, param in model_ft.named_parameters():
        if param.requires_grad == True:
            params_to_update.append(param)
            logging.info(name)
else:
    for name, param in model_ft.named_parameters():
        if param.requires_grad == True:
            logging.info(name)

# Observe that all parameters are being optimized
optimizer_ft = optim.Adam(params_to_update)


######################################################################
# Run Training and Validation Step
# --------------------------------

# Setup the loss fxn
criterion = nn.CrossEntropyLoss()

# Train and evaluate
model_ft, hist = train_model(
    model_ft, dataloaders_dict, criterion, optimizer_ft, num_epochs
)
logging.info(hist)